# Solitaire
My mom loves to play this game, so I am going to create one that run in the browser for her. Please help.

To start develop:

```sh
  $ cd solitaire
  $ gulp dev
```

To build project:

```sh
  $ gulp build
```

For more options please visit [generator-rtjs home page](https://github.com/joelchu/generator-rtjs)


# Credits

The SVG artwork is from [here](https://code.google.com/archive/p/vector-playing-cards/). Thank you [Byron Knoll](http://byronknoll.blogspot.co.uk/2011/03/vector-playing-cards.html)
